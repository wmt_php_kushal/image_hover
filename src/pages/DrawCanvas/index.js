import React, { Suspense } from 'react';
import type from 'prop-types';
import { CircularProgress } from '@material-ui/core';
import Canvas from 'react-canvas-polygons';
import { Button, Upload } from 'antd';

export class DrawCanvas extends React.PureComponent {
	constructor(props) {
		super(props);
		console.log("Props:",props.imageInfo);
	}

	handleCleanCanva = () => this.canva.cleanCanvas();

	render() {
		const { imageInfo, tool, onCompleteDraw, fenceData } = this.props;
		const imageInfoLength = Object.keys(imageInfo).length === 0;
		const onChange=()=>{
			console.log("AAA");
		}

		return (
			<>
				<Upload onChange={() => { onChange() }}><Button>Upload</Button></Upload>
				<Suspense fallback={<CircularProgress color="secondary" />}>
					<button variant="outlined" style={{ marginBottom: '20px' }}
						onClick={this.handleCleanCanva}
					>
						Clean Canvas
                </button>
					{imageInfoLength &&
						<CircularProgress color="secondary" />
					}
					<Canvas
						ref={canva => this.canva = canva}
						imgSrc={imageInfo.url}
						height={imageInfo.height}
						width={imageInfo.width}
						tool={tool}
						onCompleteDraw={onCompleteDraw}
						initialData={fenceData}
					/>
				</Suspense>
			</>);
	}
}

export default DrawCanvas;

DrawCanvas.propTypes = {
	tool: type.string,
	imageInfo: type.object,
};