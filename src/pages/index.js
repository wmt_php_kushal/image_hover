import { Col, Row } from 'antd';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import bgimage from '../assets/b8a-f.jpg';
import './styles.css';

const filterComp = (
  <defs>
    <filter id="blur1" filterUnits="userSpaceOnUse"
      x="-50%" y="-50%" width="200%" height="200%">

      <feDiffuseLighting result="diffOut" in="SourceGraphic" diffuseConstant="1.2" lighting-color="gold">
        <fePointLight x="400" y="400" z="150" pointsAtX="0" pointsAtY="0" pointsAtZ="0" />
      </feDiffuseLighting>
      <feComposite in="SourceGraphic" in2="diffOut" operator="arithmetic" k1="1" k2="0" k3="0" k4="0" />
    </filter>
  </defs>
);

let jsonData = [
  {
    id: '1',
    isSold: false,
  },
  {
    id: '9',
    isSold: true,
  },
  {
    id: '4',
    isSold: false,
  },
  {
    id: '5',
    isSold: true,
  },
  {
    id: '2',
    isSold: false,
  },
  {
    id: '3',
    isSold: true,
  },
  {
    id: '6',
    isSold: false,
  },
  {
    id: '7',
    isSold: true,
  },
  {
    id: '8',
    isSold: true,
  },
];


jsonData = jsonData.sort(function (a, b) {
  return a.id.localeCompare(b.id);
});

console.log("JSON:", jsonData);

export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentState: 0,
    };
  }

  componentDidMount() {
    const hoverFunction = i => {
      console.log('insidehover');
      this.setState({
        currentState: i,
      });
    };

    console.log('childNode:', document.getElementById('Layer_2').getAttribute('data-name'));
    const list = document.getElementById('Layer_2').childElementCount;
    let h = document.getElementById("Layer_2");
    h.insertAdjacentHTML("afterend", '<defs><filter id="blur1" filterUnits="userSpaceOnUse" x="-50%" y="-50%" width="200%" height="200%"><feDiffuseLighting result="diffOut" in="SourceGraphic" diffuseConstant="1.2" lighting-color="gold"><fePointLight x="400" y="400" z="150" pointsAtX="0" pointsAtY="0" pointsAtZ="0" /></feDiffuseLighting><feComposite in="SourceGraphic" in2="diffOut" operator="arithmetic" k1="1" k2="0" k3="0" k4="0" /></filter></defs>');
    // console.log(document.getElementById('Layer_2').insertBefore(filterComp,document.getElementById('Layer_2')));
    for (let index = 0; index < list; index++) {
      document.getElementById('Layer_2').childNodes[index].onclick = function () {
        hoverFunction(jsonData[index].id);
      };
      document.getElementById('Layer_2').childNodes[index].onmouseclick = function () {
        hoverFunction(jsonData[index].id);
      };
      document
        .getElementById('Layer_2')
        .childNodes[index].setAttribute(
          'class',
          hoverFunction(jsonData[index].isSold ? 'colorHover' : 'classUnsold'),
        );
      console.log(
        document.getElementById('Layer_2').childNodes[index].setAttribute('id', jsonData[index].id),
      );
      document
        .getElementById('Layer_2')
        .childNodes[index].setAttribute(
          'mouseClick',
          this.setState({ currentState: jsonData[index].id }),
        );
      if (jsonData[index].isSold) {
        document.getElementById('Layer_2').childNodes[index].setAttribute('class', 'classUnsold');
        document.getElementById('Layer_2').childNodes[index].setAttribute('filter', 'url(#blur1)');
      } else {
        document.getElementById('Layer_2').childNodes[index].setAttribute('class', 'colorHover');
      }
    }
    console.log(list);
    console.log('blur1:',document.getElementById('blur1'));
    document.getElementById('Layer_2').getAttribute('data-name');
  }
  render() {

    return (
      <>
        <Row>
          <Col lg={6}></Col>
          <Col lg={12}>
            <div class="container">
              {/*  */}
              <img src={bgimage} className="bg_image" />
              <div className="main-roof" id="upperDiv">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 795 620">
                  <g id="Layer_2" data-name="Layer 2">
                    <polygon
                      points="749.7 170.78 749.7 221.2 523.04 199.83 519.02 199.45 518.95 199.45 518.95 133.47 749.7 170.78"
                      fill="red"
                      opacity="0.4"
                    />

                    <polygon
                      points="749.7 226.22 749.7 275.31 523.11 266.51 518.95 266.35 518.95 204.47 749.7 226.22"
                      fill="red"
                      opacity="0.4"
                    />
                    <polygon
                      points="518.95 271.35 749.7 280.31 749.7 335.05 518.95 335.05 518.95 271.35"
                      fill="red"
                      opacity="0.4"
                    />
                    <polygon
                      points="518.95 133.47 518.95 199.45 510.73 199.95 347.07 209.82 347.07 149.13 518.95 133.47"
                      fill="#eafc06"
                      opacity="0.4"
                    />
                    <polygon
                      points="518.95 204.46 518.95 266.35 517.55 266.29 517.53 266.29 509.53 266.47 347.07 270 347.07 214.83 518.91 204.46 518.95 204.46"
                      fill="#eafc06"
                      opacity="0.4"
                    />
                    <polygon
                      points="518.95 271.35 518.95 335.05 347.07 333.96 347.07 275 517.39 271.29 517.47 271.29 518.95 271.35"
                      fill="#eafc06"
                      opacity="0.4"
                    />
                    <polygon
                      points="347.07 150.29 347.07 209.82 195.24 218.97 195.24 165.96 347.07 150.29"
                      fill="#0025ff"
                      opacity="0.4"
                    />
                    <polygon
                      points="347.07 214.83 347.07 270 195.24 273.3 195.24 223.98 347.07 214.83"
                      fill="#0025ff"
                      opacity="0.4"
                    />
                    <polygon
                      points="195.24 278.3 347.07 275 347.07 334.14 195.24 333.04 195.24 278.3"
                      fill="#0025ff"
                      opacity="0.4"
                    />
                  </g>
                </svg>
              </div>
            </div>
          </Col>
        </Row>
        <Row>
          <Col lg={12}>
            <div
              style={{
                backgroundColor: `ff00${this.state.currentState}0`,
                height: '100px',
                width: '100px',
              }}
            >
              {this.state.currentState === 9 ? (
                <div style={{ backgroundColor: 'red', height: '100px', width: '100px' }}>
                  {this.state.currentState}
                </div>
              ) : this.state.currentState === 8 ? (
                <div style={{ backgroundColor: 'blue', height: '100px', width: '100px' }}>
                  {this.state.currentState}
                </div>
              ) : (
                    <div style={{ backgroundColor: 'yellow', height: '100px', width: '100px' }}>
                      {this.state.currentState}
                    </div>
                  )}
            </div>
          </Col>
        </Row>
      </>
    );
  }
}

export default index;
