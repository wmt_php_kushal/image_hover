import styles from './index.css';
import React, { useState } from "react";
import image from '../assets/b8a-f.jpg';

export default function () {
	window.global = new Object();
	const [flatId, setflatId] = useState();
	// -- Sub menu matrix
	window.menuItems = { "menu_item_2392": [{ "permalink": "https:\/\/gartnerkvartalet.no\/boligvelger\/", "name": "Prisliste", "slug": "prisliste" }], "menu_item_2396": [{ "permalink": "https:\/\/gartnerkvartalet.no\/innredning\/", "name": "Interi\u00f8r", "slug": "interior" }], "menu_item_2395": [{ "permalink": "https:\/\/gartnerkvartalet.no\/uterom\/", "name": "Balkonger", "slug": "balkonger" }, { "permalink": "https:\/\/gartnerkvartalet.no\/uterom\/", "name": "Felles uterom", "slug": "felles-uterom" }, { "permalink": "https:\/\/gartnerkvartalet.no\/uterom\/", "name": "Aktivitet og lek", "slug": "aktivitet-og-lek" }, { "permalink": "https:\/\/gartnerkvartalet.no\/uterom\/", "name": "Gr\u00f8nne elementer", "slug": "gronne-elementer" }], "menu_item_2397": [{ "permalink": "https:\/\/gartnerkvartalet.no\/fasiliteter\/", "name": "Gr\u00f8nne oppganger", "slug": "gronne-oppganger" }, { "permalink": "https:\/\/gartnerkvartalet.no\/fasiliteter\/", "name": "Parkering", "slug": "parkering" }, { "permalink": "https:\/\/gartnerkvartalet.no\/fasiliteter\/", "name": "Barnehager", "slug": "barnehager" }, { "permalink": "https:\/\/gartnerkvartalet.no\/fasiliteter\/", "name": "N\u00e6ringslokaler", "slug": "naeringslokaler" }], "menu_item_2393": [{ "permalink": "https:\/\/gartnerkvartalet.no\/omradet\/", "name": "L\u00f8ren", "slug": "loren" }, { "permalink": "https:\/\/gartnerkvartalet.no\/omradet\/", "name": "Hovinbyen", "slug": "hovinbyen" }, { "permalink": "https:\/\/gartnerkvartalet.no\/omradet\/", "name": "Kollektivtilbud", "slug": "kollektivtilbud" }, { "permalink": "https:\/\/gartnerkvartalet.no\/omradet\/", "name": "Skole og barnehage", "slug": "skole-og-barnehage" }], "menu_item_2394": [{ "permalink": "https:\/\/gartnerkvartalet.no\/contact\/", "name": "Kontakt oss", "slug": "kontakt-oss" }, { "permalink": "https:\/\/gartnerkvartalet.no\/contact\/", "name": "Dokumenter", "slug": "dokumenter" }, { "permalink": "https:\/\/gartnerkvartalet.no\/contact\/", "name": "Om utbyggerne", "slug": "om-utbyggerne" }] };
	window.url = 'https://gartnerkvartalet.no';
	window.api = 'https://gartnerkvartalet.no/wp-content/themes/gartnerkvartalet';

	(function () {
		var _fbq = window._fbq || (window._fbq = []);
		if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
		}
		_fbq.push(['addPixelId', '765708506858617']);
	})();
	window._fbq = window._fbq || [];
	window._fbq.push(['track', 'PixelInitialized', {}]);

	function onload() {
		let content = document.getElementById('building-7-front');
		let firstChild = content?.childNodes[0].firstChild;
		let tmpVar = content?.childNodes[0].childNodes;
		console.log("tmpVar:", (tmpVar)?.length);
		for (let i = 0; i < tmpVar?.length; i++) {
			console.log("firstChild:", tmpVar[i]?.getAttribute('id'));
			// tmpVar[i].childNodes
			// tmpVar[i]?.setAttribute("onClick", ()=>console.log(i));
		}
		// tmpVar.forEach(i=>tmpVar[i]?.setAttribute("onHover", `{()=>setFlatId("${(tmpVar[i]?.getAttribute("id"))}")}`));
		console.log("After adding:", firstChild);
		console.log("flatId:", flatId);
	}
	let content = document.getElementById('building-7-front');
	let tmpVar = content?.childNodes[0].childNodes;
	console.log("tmpVar:", tmpVar?.length);
	// tmpVar?.getAttribute('id');
	// let tmpTag=document.getElementById('building-7-front');
	// console.log("tmpTag", document.getElementById('building-7-front').children.hasAttribute("data-id"));


	(function (win, doc, sdk_url) {
		if (win.snaptr) return;
		var tr = win.snaptr = function () {
			tr.handleRequest ? tr.handleRequest.apply(tr, arguments) : tr.queue.push(arguments);
		};
		tr.queue = [];
		var s = 'script';
		var new_script_section = doc.createElement(s);
		new_script_section.async = !0;
		new_script_section.src = sdk_url;
		var insert_pos = doc.getElementsByTagName(s)[0];
		insert_pos.parentNode.insertBefore(new_script_section, insert_pos);
	})(window, document, 'https://sc-static.net/scevent.min.js');



	return (
		
		<div id="apartment-splash" data-permalink="https://gartnerkvartalet.no/apartment/8703/">

			<link rel='stylesheet' id='wp-block-library-css' href='https://gartnerkvartalet.no/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4' type='text/css' media='all' />
			<link rel='stylesheet' id='cookie-law-info-css' href='https://gartnerkvartalet.no/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-public.css?ver=1.8.8' type='text/css' media='all' />
			<link rel='stylesheet' id='cookie-law-info-gdpr-css' href='https://gartnerkvartalet.no/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-gdpr.css?ver=1.8.8' type='text/css' media='all' />
			<link rel='stylesheet' id='cookie-notice-front-css' href='https://gartnerkvartalet.no/wp-content/plugins/cookie-notice/css/front.min.css?ver=5.4.4' type='text/css' media='all' />
			{/* <link rel='stylesheet' id='sbscrbr_style-css'  href='https://gartnerkvartalet.no/wp-content/plugins/subscriber/css/frontend_style.css?ver=5.4.4' type='text/css' media='all' />
<link rel='stylesheet' id='theme-font-roboto-css'  href='//fonts.googleapis.com/css?family=Roboto+Slab%3A400%2C700%2C300&#038;ver=1530198316' type='text/css' media='screen' /> */}
			<link rel='stylesheet' id='theme-font-din-css' href='https://gartnerkvartalet.no/wp-content/themes/gartnerkvartalet/assets/fonts/din/font.css?ver=1332360131' type='text/css' media='screen' />
			{/* This the line which sets image in background */}
			<link rel='stylesheet' id='theme-style-css' href='https://gartnerkvartalet.no/wp-content/themes/gartnerkvartalet/assets/styles/main.css?ver=1610775819' type='text/css' media='screen' />
			{/*  */}
			<link rel='stylesheet' id='moove_gdpr_frontend-css' href='https://gartnerkvartalet.no/wp-content/plugins/gdpr-cookie-compliance/dist/styles/gdpr-main.css?ver=4.1.9' type='text/css' media='all' />
			<script type='text/javascript' src='https://gartnerkvartalet.no/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.3.1'></script>
			<link rel='https://api.w.org/' href='https://gartnerkvartalet.no/wp-json/' />
			<link rel='shortlink' href='https://gartnerkvartalet.no/?p=4880' />
			<link rel="alternate" type="application/json+oembed" href="https://gartnerkvartalet.no/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fgartnerkvartalet.no%2Fboligvelger%2Fhus-7%2F" />
			<link rel="alternate" type="text/xml+oembed" href="https://gartnerkvartalet.no/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fgartnerkvartalet.no%2Fboligvelger%2Fhus-7%2F&#038;format=xml" />
			<div class="wrap clearfix" onLoad={onload}>
				<div class="inner">
					<div class="building">
						{/* <a href="#turn-back" class="turn-back">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="78px" height="78px" viewBox="0 0 78 78" enable-background="new 0 0 78 78" xmlSpace="preserve">
							<path fill="#ffffff" d="M65.253,50.831l-11.716,4.588l3.483,2.781c-4.783,4.851-11.299,7.635-18.319,7.635 c-9.066,0-17.303-4.627-22.035-12.377l-3.414,2.084c5.463,8.95,14.977,14.293,25.449,14.293c8.258,0,15.91-3.333,21.46-9.127 l3.209,2.562L65.253,50.831z"></path>
						</svg>
						<strong>Snu Bygget</strong>
					</a> */}


						<div class="front" id="building-7-front">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 795 620"><g id="Layer_2" data-name="Layer 2">
								<polygon points="749.7 170.78 749.7 221.2 523.04 199.83 519.02 199.45 518.95 199.45 518.95 133.47 749.7 170.78" fill="red" opacity="0.4" />
								<polygon points="749.7 226.22 749.7 275.31 523.11 266.51 518.95 266.35 518.95 204.47 749.7 226.22" fill="red" opacity="0.4" />
								<polygon points="518.95 271.35 749.7 280.31 749.7 335.05 518.95 335.05 518.95 271.35" fill="red" opacity="0.4" />
								<polygon points="518.95 133.47 518.95 199.45 510.73 199.95 347.07 209.82 347.07 149.13 518.95 133.47" fill="#eafc06" opacity="0.4" />
								<polygon points="518.95 204.46 518.95 266.35 517.55 266.29 517.53 266.29 509.53 266.47 347.07 270 347.07 214.83 518.91 204.46 518.95 204.46" fill="#eafc06" opacity="0.4" />
								<polygon points="518.95 271.35 518.95 335.05 347.07 333.96 347.07 275 517.39 271.29 517.47 271.29 518.95 271.35" fill="#eafc06" opacity="0.4" />
								<polygon points="347.07 150.29 347.07 209.82 195.24 218.97 195.24 165.96 347.07 150.29" fill="#0025ff" opacity="0.4" />
								<polygon points="347.07 214.83 347.07 270 195.24 273.3 195.24 223.98 347.07 214.83" fill="#0025ff" opacity="0.4" />
								<polygon points="195.24 278.3 347.07 275 347.07 334.14 195.24 333.04 195.24 278.3" fill="#0025ff" opacity="0.4" /></g></svg>
						</div>
						<div class="back hidden" id="building-7-back">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="826.66669" width="1060" viewBox="0 0 1060 826.66669" enable-background="new 0 0 1060 826.66669" xmlSpace="preserve">
								<path class="flat attached unavailable" id="b7-b-1" d="m 54.666665,155.77802 v 54.056 L 164,207.11135 l 21.45866,-0.028 44.6,-0.416 v -59.33333 l -44.72533,2.944 -22.22267,-2.27733 z" data-id="4859" data-permalink="https://gartnerkvartalet.no/apartment/7906/"></path>
								<path class="flat attached unavailable" id="b7-b-2" d="m 54.666665,213.83335 v 55.084 l 175.167995,-2.14 0.22533,-55.85067 h -44.28266 l -22.66667,0.184 z" data-id="4853" data-permalink="https://gartnerkvartalet.no/apartment/7806/"></path>
								<path class="flat attached unavailable" id="b7-b-3" d="m 54.666665,272.83335 v 56.61067 l 175.167995,-2 0.228,-56.58134 -66.952,0.91467 z" data-id="4847" data-permalink="https://gartnerkvartalet.no/apartment/7706/"></path>
								<path class="flat attached unavailable" id="b7-b-4" d="m 54.666665,333.63868 v 56.61067 l 107.774665,0.528 H 185.556 l 44.27866,-0.66534 0.228,-58.444 -66.952,0.91467 z" data-id="4838" data-permalink="https://gartnerkvartalet.no/apartment/7606/"></path>
								<path class="flat attached unavailable" id="b7-b-5" d="m 54.666665,394.42188 v 52.68933 l 108.110665,1.44534 23.14,-0.80667 43.91733,0.028 0.228,-53.61067 -66.54933,0.58267 z" data-id="4832" data-permalink="https://gartnerkvartalet.no/apartment/7506/"></path>
								<path class="flat attached unavailable" id="b7-b-6" d="m 54.666665,450.22268 v 55.888 l 108.110665,6.056 22.77867,-2.27733 44.27733,0.22133 0.22933,-58.22 -44.396,0.0267 -22.15333,0.63866 z" data-id="4825" data-permalink="https://gartnerkvartalet.no/apartment/7406/"></path>
								<path class="flat attached unavailable" id="b7-b-7" d="m 54.666665,510.16668 v 55.056 l 108.110665,6.66667 22.556,-2.056 44.5,0.72266 0.22933,-56.16666 h -44.396 l -22.15333,1.888 z" data-id="4817" data-permalink="https://gartnerkvartalet.no/apartment/7306/"></path>
								<path class="flat attached unavailable" id="b7-b-8" d="m 54.666665,569.33334 v 52.83333 l 108.110665,9.38934 22.64,-6.056 44.61733,-0.16667 0.028,-50.668 -44.396,-0.60933 -22.15333,1.888 z" data-id="4807" data-permalink="https://gartnerkvartalet.no/apartment/7206/"></path>
								<path class="flat attached unavailable" id="b7-b-9" d="m 54.666665,626.33334 v 61.66667 l 71.999995,23.66666 38.66667,2 49.83333,-16.83333 10.67333,-48.85867 4.22267,-1.308 v -17 h -44.22933 l -23.16667,6.16667 z" data-id="4779" data-permalink="https://gartnerkvartalet.no/apartment/7106/"></path>
								<path class="flat attached unavailable" id="b7-b-10" d="m 234.05533,574.88934 19.056,0.63867 0.27467,7.612 98.77999,11.08266 38.51467,-9.01466 33.764,0.792 9.708,2.38133 0.21333,63.66 -9.94933,1.27066 -40.712,-3.36533 -31.26,9.49733 -92.36533,-16.916 -0.19067,-13.85866 -25.83333,-3.05867 z" data-id="4788" data-permalink="https://gartnerkvartalet.no/apartment/7202/"></path>
								<path class="flat attached unavailable" id="b7-b-11" d="m 245.77799,688.88934 5.376,-30.372 15.29067,-4.29467 -0.21867,-6.248 86.52,15.71467 31.75466,-9.68933 39.93734,3.37466 9.928,-1.29066 v 53.33333 l -9.92134,4.24933 -34.77733,-5.66666 -44.33333,19.33333 -111.27867,-27.66667 z" data-id="4762" data-permalink="https://gartnerkvartalet.no/apartment/7102/"></path>
								<path class="flat attached unavailable" id="b7-b-12" d="m 424.44466,125.36069 -34.61067,3.5 -37.72399,-8.51067 -91.184,14.57067 -0.31867,9.08 -26.552,2.95733 v 59.09733 l 190.65333,-16.97199 9.65733,2.63066 -0.0973,-63.104 z" data-id="4855" data-permalink="https://gartnerkvartalet.no/apartment/7902/"></path>
								<path class="flat attached unavailable" id="b7-b-13" d="m 424.44466,193.16669 -190.38933,17.22933 v 56.41067 l 118.57467,-6.47334 31.37066,0.5 40.444,-0.66666 9.892,-0.16667 0.0293,-64.052 z" data-id="4849" data-permalink="https://gartnerkvartalet.no/apartment/7802/"></path>
								<path class="flat attached unavailable" id="b7-b-14" d="m 424.44466,264.22202 -37.11067,1.11067 -34.44533,-0.33334 -118.83333,5.86267 v 56.69333 l 118.27867,-0.11066 72.11066,-0.112 9.92133,0.0467 v -63.15733 z" data-id="4841" data-permalink="https://gartnerkvartalet.no/apartment/7702/"></path>
								<path class="flat attached unavailable" id="b7-b-15" d="M 424.44466,331.52935 H 234.05533 v 58.588 h 190.38933 9.92133 v -58.588 z" data-id="4834" data-permalink="https://gartnerkvartalet.no/apartment/7602/"></path>
								<path class="flat attached unavailable" id="b7-b-16" d="m 424.44466,394.12375 -190.38933,0.0427 v 53.832 l 24.61067,0.0827 2.068,6.11867 92.6,1.46667 30.66666,-3.22267 40.444,0.44533 9.92133,0.16134 v -58.92667 z" data-id="4827" data-permalink="https://gartnerkvartalet.no/apartment/7502/"></path>
								<path class="flat attached unavailable" id="b7-b-17" d="m 424.44466,456.91668 -40.11067,-0.36133 -30.99866,3.30266 -96.02933,-2.65066 -1.52534,-4.968 -21.72533,-0.24134 v 58.28 l 22.83333,0.83334 0.0147,4.60666 96.43067,6.116 35.74933,-4.16666 35.36133,0.55466 9.92133,0.83467 v -61.996 z" data-id="4821" data-permalink="https://gartnerkvartalet.no/apartment/7402/"></path>
								<path class="flat attached unavailable" id="b7-b-18" d="m 424.44466,522.33334 -34.11067,-0.5 -38.16666,4.25067 -99.25067,-6.584 -0.33333,-4.5 -18.528,-0.58267 v 56.24933 l 23.112,0.78534 0.24933,8.21466 95.13867,10.27733 38.45733,-8.83866 33.432,0.83867 9.92133,2.376 v -61.152 z" data-id="4813" data-permalink="https://gartnerkvartalet.no/apartment/7302/"></path>
								<path class="flat attached unavailable" id="b7-b-19" d="m 438.24999,128.83335 v 63.38933 l 126.084,-3.88933 30.77733,0.33333 v -30.5 l 21.77733,-0.5 v -33.112 l -22.108,-0.10266 -0.11333,-1.78534 -16.64,1.66667 -15.36,-4.972 z" data-id="4854" data-permalink="https://gartnerkvartalet.no/apartment/7901/"></path>
								<path class="flat attached unavailable" id="b7-b-20" d="m 438.24999,196.33335 v 63.77733 l 125.084,-3.63866 15.176,0.86133 14.712,-0.36133 v -18.30534 l 23.66666,0.056 v -41.14 l -21.77733,0.084 v -4.5 z" data-id="4848" data-permalink="https://gartnerkvartalet.no/apartment/7801/"></path>
								<path class="flat attached unavailable" id="b7-b-21" d="m 438.24999,264.22202 v 63.77733 l 125.41733,-1.77733 15.22133,-0.22267 14.33334,-0.22266 v -16.73467 l 23.66666,0.0413 v -32.64 l -23.444,-0.11066 -0.22133,-15.27734 z" data-id="4839" data-permalink="https://gartnerkvartalet.no/apartment/7701/"></path>
								<path class="flat attached unavailable" id="b7-b-22" d="m 438.24999,331.83335 v 58.284 l 125.30533,1.66 h 16.88933 12.77734 v -11.944 l 23.66666,0.16667 v -33.00134 l -23.444,0.003 -0.22133,-16.89067 z" data-id="4833" data-permalink="https://gartnerkvartalet.no/apartment/7601/"></path>
								<path class="flat attached unavailable" id="b7-b-23" d="m 438.24999,394.12375 v 58.76533 l 125.30533,2.84 15.44533,-1.06266 14.22134,-0.001 v -5.444 l 23.66666,0.056 v -31.72266 l -22.22133,-0.444 -0.112,-20.94534 -31.884,0.33467 z" data-id="4826" data-permalink="https://gartnerkvartalet.no/apartment/7501/"></path>
								<path class="flat attached unavailable" id="b7-b-24" d="m 438.24999,457.25001 v 61.91733 l 123.48933,4.63867 17.26133,-2.51467 14.22134,0.152 23.66666,0.36134 v -32.472 l -22.16533,0.16666 -0.112,-30.66666 -15.19467,-0.0827 -16.42,1.08 z" data-id="4819" data-permalink="https://gartnerkvartalet.no/apartment/7401/"></path>
								<path class="flat attached unavailable" id="b7-b-25" d="m 438.24999,523.33334 v 61.66667 l 123.48933,6.66666 17.26133,-1.11067 37.888,0.332 v -30.38799 l -22.16533,-0.27734 -0.112,-34.83333 -15.19467,-0.084 -16.972,2.69467 z" data-id="4812" data-permalink="https://gartnerkvartalet.no/apartment/7301/"></path>
								<path class="flat attached unavailable" id="b7-b-26" d="m 438.24999,589.00001 v 63 l 125.66666,9.16666 15.084,-3.83333 37.888,1.584 v -28.41733 l -22.16533,0.003 -0.112,-35.50267 -15.19467,-0.16667 -16.972,1 z" data-id="4784" data-permalink="https://gartnerkvartalet.no/apartment/7201/"></path>
								<path class="flat attached unavailable" id="b7-b-27" d="m 438.24999,656.11067 v 63 l 121.032,18.06533 36.38533,-18.84133 -0.12267,-50.89067 11.56667,-4.888 -12.5,-0.44533 -15.19467,-0.16667 -16.972,3.38934 z" data-id="4757" data-permalink="https://gartnerkvartalet.no/apartment/7101/"></path>
								<path class="flat attached unavailable" id="b7-b-28" d="m 995.58064,76.110022 v -16.5 L 966.91397,35.77669 792.91398,55.110023 v -6.333334 l -12.66667,1.333334 -0.33333,-2 -28.66667,-15 -26.66666,2.666667 -6.66667,-4.333334 h -8.33333 l -33,-15.999999 -213,31.333332 -36.74533,-13.474666 -192.77999,29.253333 v 80.277334 l 22.48666,-2.54134 0.208,-8.87066 95.64533,-14.796 37.54934,8.208 34.94399,-3.556 11.69467,3.80666 126.416,-9.86133 15.45866,5.028 198.37467,-17.91733 205.83332,-20.000001 15.58134,9.943999 V 78.776689 Z" data-id="4864" data-permalink="https://gartnerkvartalet.no/apartment/71002/"></path>
								<path class="flat attached unavailable" id="b7-b-29" d="M 187.55533,70.222689 54.666,89.777355 v 61.889335 l 108,-7.66667 22.584,2.584 44.72533,-3.33333 0.084,-80.088001 -42.504,6.449333 z" data-id="4883" data-permalink="https://gartnerkvartalet.no/apartment/71006/"></path>
							</svg>
						</div>

					</div>
					<div class="aside">
						<div class="inner">
							<h2>Gjennomgående 94,5 m² leilighet med to balkonger og mulighet for hybel</h2>
							<h3>Leilighet <strong>8404</strong></h3>
							<h3>Leilighetstype <strong>Q</strong></h3>
							<p><strong>Totalpris:</strong> 8.150.000,-</p>
							<p><strong>Antall rom:</strong> 4</p>
							<p><strong>Etasje:</strong> 4</p><p><strong>BRA:</strong> 94 m²</p>
							<p><strong>P-rom:</strong> 91 m²</p><p><strong>Balkong:</strong> 8,5  m² +  4,2 m²</p>
							<p><strong>Parkeringsplass:</strong> Ja</p><p><strong>Innflytting</strong> 25.05.21 - 25.08.21</p><div class="floor-plan">
								<img src={image} alt="Floor plan" />
							</div>
						</div>
					</div>
				</div>


			</div>
		</div >
	);
}

// import React, { Component } from "react";
// import ImageMap from "image-map";

// ImageMap('img[usemap]');

// const URL = "https://c1.staticflickr.com/5/4052/4503898393_303cfbc9fd_b.jpg"
// const MAP = {
// 	name: "my-map",
// 	areas: [
// 		{ name: "1", shape: "poly", coords: [25, 33, 27, 300, 128, 240, 128, 94], preFillColor: "green", fillColor: "blue" },
// 		{ name: "2", shape: "poly", coords: [219, 118, 220, 210, 283, 210, 284, 119], preFillColor: "pink" },
// 		{ name: "3", shape: "poly", coords: [381, 241, 383, 94, 462, 53, 457, 282], fillColor: "yellow" },
// 		{ name: "4", shape: "poly", coords: [245, 285, 290, 285, 274, 239, 249, 238], preFillColor: "red" },
// 		{ name: "5", shape: "circle", coords: [170, 100, 25] },
// 	]
// }
// export default class ImageMapper extends Component {

// 	constructor(props) {

// 		super(props);
// 		this.state = {
// 			hoveredArea: {},
// 		};
// 	}

// 	selectRegion(ctx, region) {
// 		var selector = 'img[usemap=\'#' + ctx.parentElement.name + '\']';
// 		document.querySelector(selector).setAttribute('src', 'image-map-' + region + '.png');
// 	}

// 	enterArea(area) {
// 		this.setState({ hoveredArea: area });
// 	}

// 	leaveArea(area) {
// 		this.setState({ hoveredArea: null });
// 	}

// 	getTipPosition(area) {
// 		return { top: `${area.center[1]}px`, left: `${area.center[0]}px` };
// 	}


// 	render() {
// 		return (
// 			<>
// <div id="greenhouse">
//     <svg id="lines" viewBox="0 0 1280 1280" 
// preserveAspectRatio="xMinYMin meet">
//         <path stroke="#d4ffde" stroke-width="3" fill="none"  d="
//             M 130,820
//             L 130,320
//             l 50,0
//         "/>
//         <path stroke="#d4ffde" stroke-width="3" fill="none" d="
//             M 620,820
//             L 620,520
//             l -230,0
//         "/>
//         <path stroke="#d4ffde" stroke-width="3" fill="none" d="
//             M 1100,820
//             L 1100,380                         
//             l -290,0
//         "/>
//     </svg>
//     <svg viewBox="0 0 1280 720" 
//     preserveAspectRatio="xMinYMin meet">
//         <image width="1280" 
//         height="720" xlink="https://dkli3tbfz4zj3.cloudfront.net/all/202006_SaladDays/images/greenhouse.jpg">
//         </image>
//         <circle cx="220" cy="320" r="40" stroke="#d4ffde" 
//         stroke-width="3" fill="none" />
//         <circle cx="320" cy="520" r="70" stroke="#d4ffde" 
//         stroke-width="3" fill="none" />
//         <circle cx="750" cy="380" r="60" stroke="#d4ffde" 
//         stroke-width="3" fill="none" />
//     </svg>
//     <div id="greenhouse-details">
//         <div class="narrow-text">
//             <p>Whip up a delicious salad</p>
//         </div>
//         <div class="narrow-text">
//             <p>Cuddle up to Marguerit’s Snow Stalker companion</p>
//         </div>
//         <div class="narrow-text">
//             <p>Learn how to farm new, unusual plants</p>
//         </div>
//     </div>
// </div>
// 				{/* <img usemap="#image-map" src="https://www.travismclarke.com/imagemap/image-map-all.png" onClick={()=>this.selectRegion(this, 'yellow')} />

// 				<map name="image-map">
// 					<area shape="poly" coords="22,22,231,22,264,82,232,143,22,143" />
// 					<area shape="poly" coords="233,22,443,22,476,82,442,144,233,143,264,82" />
// 					<area shape="poly" coords="445,22,654,22,686,81,654,143,444,143,475,82" />
// 					<area shape="poly" coords="655,22,895,22,895,142,655,142,684,82" />
// 				</map> */}

// 			</>);
// 	}


// };*/