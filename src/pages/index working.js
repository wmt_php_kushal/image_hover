import styles from './index.css';

export default function () {
	window.global = new Object();

	// -- Sub menu matrix
	window.menuItems = { "menu_item_2392": [{ "permalink": "https:\/\/gartnerkvartalet.no\/boligvelger\/", "name": "Prisliste", "slug": "prisliste" }], "menu_item_2396": [{ "permalink": "https:\/\/gartnerkvartalet.no\/innredning\/", "name": "Interi\u00f8r", "slug": "interior" }], "menu_item_2395": [{ "permalink": "https:\/\/gartnerkvartalet.no\/uterom\/", "name": "Balkonger", "slug": "balkonger" }, { "permalink": "https:\/\/gartnerkvartalet.no\/uterom\/", "name": "Felles uterom", "slug": "felles-uterom" }, { "permalink": "https:\/\/gartnerkvartalet.no\/uterom\/", "name": "Aktivitet og lek", "slug": "aktivitet-og-lek" }, { "permalink": "https:\/\/gartnerkvartalet.no\/uterom\/", "name": "Gr\u00f8nne elementer", "slug": "gronne-elementer" }], "menu_item_2397": [{ "permalink": "https:\/\/gartnerkvartalet.no\/fasiliteter\/", "name": "Gr\u00f8nne oppganger", "slug": "gronne-oppganger" }, { "permalink": "https:\/\/gartnerkvartalet.no\/fasiliteter\/", "name": "Parkering", "slug": "parkering" }, { "permalink": "https:\/\/gartnerkvartalet.no\/fasiliteter\/", "name": "Barnehager", "slug": "barnehager" }, { "permalink": "https:\/\/gartnerkvartalet.no\/fasiliteter\/", "name": "N\u00e6ringslokaler", "slug": "naeringslokaler" }], "menu_item_2393": [{ "permalink": "https:\/\/gartnerkvartalet.no\/omradet\/", "name": "L\u00f8ren", "slug": "loren" }, { "permalink": "https:\/\/gartnerkvartalet.no\/omradet\/", "name": "Hovinbyen", "slug": "hovinbyen" }, { "permalink": "https:\/\/gartnerkvartalet.no\/omradet\/", "name": "Kollektivtilbud", "slug": "kollektivtilbud" }, { "permalink": "https:\/\/gartnerkvartalet.no\/omradet\/", "name": "Skole og barnehage", "slug": "skole-og-barnehage" }], "menu_item_2394": [{ "permalink": "https:\/\/gartnerkvartalet.no\/contact\/", "name": "Kontakt oss", "slug": "kontakt-oss" }, { "permalink": "https:\/\/gartnerkvartalet.no\/contact\/", "name": "Dokumenter", "slug": "dokumenter" }, { "permalink": "https:\/\/gartnerkvartalet.no\/contact\/", "name": "Om utbyggerne", "slug": "om-utbyggerne" }] };
	window.url = 'https://gartnerkvartalet.no';
	window.api = 'https://gartnerkvartalet.no/wp-content/themes/gartnerkvartalet';

	(function () {
		var _fbq = window._fbq || (window._fbq = []);
		if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
		}
		_fbq.push(['addPixelId', '765708506858617']);
	})();
	window._fbq = window._fbq || [];
	window._fbq.push(['track', 'PixelInitialized', {}]);


	(function (win, doc, sdk_url) {
		if (win.snaptr) return;
		var tr = win.snaptr = function () {
			tr.handleRequest ? tr.handleRequest.apply(tr, arguments) : tr.queue.push(arguments);
		};
		tr.queue = [];
		var s = 'script';
		var new_script_section = doc.createElement(s);
		new_script_section.async = !0;
		new_script_section.src = sdk_url;
		var insert_pos = doc.getElementsByTagName(s)[0];
		insert_pos.parentNode.insertBefore(new_script_section, insert_pos);
	})(window, document, 'https://sc-static.net/scevent.min.js');



	return (
		<div id="apartment-splash" data-permalink="https://gartnerkvartalet.no/apartment/8703/">

			<link rel='stylesheet' id='wp-block-library-css' href='https://gartnerkvartalet.no/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4' type='text/css' media='all' />
			<link rel='stylesheet' id='cookie-law-info-css' href='https://gartnerkvartalet.no/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-public.css?ver=1.8.8' type='text/css' media='all' />
			<link rel='stylesheet' id='cookie-law-info-gdpr-css' href='https://gartnerkvartalet.no/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-gdpr.css?ver=1.8.8' type='text/css' media='all' />
			<link rel='stylesheet' id='cookie-notice-front-css' href='https://gartnerkvartalet.no/wp-content/plugins/cookie-notice/css/front.min.css?ver=5.4.4' type='text/css' media='all' />
			{/* <link rel='stylesheet' id='sbscrbr_style-css'  href='https://gartnerkvartalet.no/wp-content/plugins/subscriber/css/frontend_style.css?ver=5.4.4' type='text/css' media='all' />
<link rel='stylesheet' id='theme-font-roboto-css'  href='//fonts.googleapis.com/css?family=Roboto+Slab%3A400%2C700%2C300&#038;ver=1530198316' type='text/css' media='screen' /> */}
			<link rel='stylesheet' id='theme-font-din-css' href='https://gartnerkvartalet.no/wp-content/themes/gartnerkvartalet/assets/fonts/din/font.css?ver=1332360131' type='text/css' media='screen' />
			{/* This the line which sets image in background */}
			<link rel='stylesheet' id='theme-style-css' href='https://gartnerkvartalet.no/wp-content/themes/gartnerkvartalet/assets/styles/main.css?ver=1610775819' type='text/css' media='screen' />
			{/*  */}
			<link rel='stylesheet' id='moove_gdpr_frontend-css' href='https://gartnerkvartalet.no/wp-content/plugins/gdpr-cookie-compliance/dist/styles/gdpr-main.css?ver=4.1.9' type='text/css' media='all' />
			<script type='text/javascript' src='https://gartnerkvartalet.no/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.3.1'></script>
			<link rel='https://api.w.org/' href='https://gartnerkvartalet.no/wp-json/' />
			<link rel='shortlink' href='https://gartnerkvartalet.no/?p=4880' />
			<link rel="alternate" type="application/json+oembed" href="https://gartnerkvartalet.no/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fgartnerkvartalet.no%2Fboligvelger%2Fhus-7%2F" />
			<link rel="alternate" type="text/xml+oembed" href="https://gartnerkvartalet.no/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fgartnerkvartalet.no%2Fboligvelger%2Fhus-7%2F&#038;format=xml" />
			<div class="wrap clearfix">
				<div class="inner">
					<div class="building">
						{/* <a href="#turn-back" class="turn-back">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="78px" height="78px" viewBox="0 0 78 78" enable-background="new 0 0 78 78" xmlSpace="preserve">
							<path fill="#ffffff" d="M65.253,50.831l-11.716,4.588l3.483,2.781c-4.783,4.851-11.299,7.635-18.319,7.635 c-9.066,0-17.303-4.627-22.035-12.377l-3.414,2.084c5.463,8.95,14.977,14.293,25.449,14.293c8.258,0,15.91-3.333,21.46-9.127 l3.209,2.562L65.253,50.831z"></path>
						</svg>
						<strong>Snu Bygget</strong>
					</a> */}






						<div class="front" id="building-7-front">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="826.66669" width="1060" viewBox="0 0 1060 826.66669" enable-background="new 0 0 1060 826.66669" xmlSpace="preserve">
								<path class="flat attached unavailable" id="b7-f-1" d="M 966.66620,432 934.33398,432.55469 886,433.08203 670,432.5 V 498 l 216,7.77734 48.52734,-4.30859 30.69532,0.53125 57.94334,-5.33398 v -36.83204 l -56.49998,-1.61132 z" data-id="4831" data-permalink="https://gartnerkvartalet.no/apartment/7505/"></path>
								<path class="flat attached unavailable" id="b7-f-2" d="m 886,356 -216,4 v 68.5 l 216,-0.0625 48.77734,0.0625 29.7793,-0.5 58.60936,-0.88867 V 391 l -56.49998,-0.55664 V 358 l -32.33204,-0.084 z" data-id="4837" data-permalink="https://gartnerkvartalet.no/apartment/7605/"></path>
								<path class="flat attached unavailable" id="b7-f-3" d="m 670,502.08398 v 68.13868 L 886,586 934.33398,573.58398 965.41602,573.75 1023.166,563.33398 V 529.11133 L 965.45117,527.55469 965.5,506.04297 934.25,505.77734 886,510 Z" data-id="4824" data-permalink="https://gartnerkvartalet.no/apartment/7405/"></path>
								<path class="flat attached unavailable" id="b7-f-4" d="m 670,574.44336 v 62.44531 l 216,25.33399 48.33398,-15.55664 32.3418,3.77734 57.54692,-16.2207 v -31.11133 l -57.54692,-4.44531 V 578 L 934.33398,577.77734 886,590.44336 Z" data-id="4816" data-permalink="https://gartnerkvartalet.no/apartment/7305/"></path>
								<path class="flat attached unavailable" id="b7-f-5" d="m 670,641 v 64.77734 l 216,37.7793 49.77734,-23.11328 28.44532,4.66797 58.94334,-22 v -30.22266 l -57.71483,-2.22265 V 654.05664 L 935.55664,650.52734 886,667.11133 Z" data-id="4803" data-permalink="https://gartnerkvartalet.no/apartment/7205/"></path>
								<path class="flat attached unavailable" id="b7-f-6" d="m 670,709.88867 v 51.27735 l 216,45.72265 87.74023,5.55469 68.92577,-43.77734 -77.21483,-11.10938 V 729.66602 L 936.16602,724.55664 886,748 Z" data-id="4774" data-permalink="https://gartnerkvartalet.no/apartment/7105/"></path>
								<path class="flat attached unavailable" id="b7-f-7" d="m 886,275.66602 -216,12.5 v 68.22265 l 216,-4.55664 48.33398,2.33399 31.11719,1.08398 57.71483,0.75 v -37.5 l -57.71483,-2.83398 v -28 L 934.5,283.22266 Z" data-id="4845" data-permalink="https://gartnerkvartalet.no/apartment/7705/"></path>
								<path class="flat attached unavailable" id="b7-f-8" d="m 886,199.11133 -216,22.81836 v 62.51562 l 216,-13.55664 48.83398,9.36133 31,0.16602 57.33202,7.25195 v -37 l -56.49022,-10 V 218 Z" data-id="4852" data-permalink="https://gartnerkvartalet.no/apartment/7805/"></path>
								<path class="flat attached unavailable" id="b7-f-9" d="M 886,123.11133 670,153.5 v 64.45898 l 216,-22.84765 48.33398,11.33398 31.11719,-0.11133 57.71483,11.44532 v -36.22266 l -57.71483,-16 v -16 z" data-id="4858" data-permalink="https://gartnerkvartalet.no/apartment/7905/"></path>
								<path class="flat attached unavailable" id="b7-f-10" d="M 886,37.777341 670,80.249997 v 69.101563 l 216,-30.24023 48.33398,16 31.11719,-2.22266 57.71483,16.44531 V 110.39844 L 965.47656,92.443356 V 82.888669 L 1025.1914,77.412106 966.80859,56.2207 934.33398,59.333981 Z" data-id="4872" data-permalink="https://gartnerkvartalet.no/apartment/71005/"></path>
								<path class="flat attached unavailable" id="b7-f-11" d="m 507.5,629.33398 v 64.94336 l 15,1.72071 v 8.66797 l 80.16602,19.77734 39.66796,-11.33203 23.5,-7.69336 V 641 L 642,644.91797 l -39.11133,8.63672 -84.66601,-13.84961 0.2207,-9.26172 z" data-id="4799" data-permalink="https://gartnerkvartalet.no/apartment/7204/"></path>
								<path class="flat attached unavailable" id="b7-f-12" d="m 508.08203,561.22266 -0.54297,63.67382 15.29492,1.32422 v 10.22266 L 602.55664,649.33398 641.22266,641 665.83398,636.83398 V 574.25 l -23.62695,1.33203 -39.54101,3.19336 -80,-7.34766 h -4 v -9.38671 z" data-id="4815" data-permalink="https://gartnerkvartalet.no/apartment/7304/"></path>
								<path class="flat attached unavailable" id="b7-f-13" d="m 508.08203,430.55664 v 66.16602 l 94.86133,2.52734 h 39.89648 L 665.83398,498 v -65.33398 l -24.16796,-0.10938 z" data-id="4829" data-permalink="https://gartnerkvartalet.no/apartment/7504/"></path>
								<path class="flat attached unavailable" id="b7-f-14" d="m 508.08203,500.57031 v 56.65821 l 14.75195,0.9375 v 7.17968 2.27735 l 80.10938,6.82031 39.05664,-3 23.83398,-1.27734 v -68.08399 l -24.25,1.16797 -38.69531,0.084 z" data-id="4823" data-permalink="https://gartnerkvartalet.no/apartment/7404/"></path>
								<path class="flat attached unavailable" id="b7-f-15" d="m 666,360.08203 -24.41602,0.29883 -133.50195,0.97852 v 64.86328 l 94.86133,2.38867 33.89062,-0.11133 h 29 z" data-id="4836" data-permalink="https://gartnerkvartalet.no/apartment/7604/"></path>
								<path class="flat attached unavailable" id="b7-f-16" d="m 602.88867,285.05469 -94.80664,5.11133 v 67.27343 l 94.86133,-0.55078 33.89062,-0.61133 L 666,356.08398 v -67.91796 h -27.71875 z" data-id="4844" data-permalink="https://gartnerkvartalet.no/apartment/7704/"></path>
								<path class="flat attached unavailable" id="b7-f-17" d="m 602.16602,214.66602 -94.08399,9.5 v 61.94921 l 94.86133,-5.22656 36.22266,3.5 H 666 v -62.36523 l -24.55664,-3.35742 z" data-id="4851" data-permalink="https://gartnerkvartalet.no/apartment/7804/"></path>
								<path class="flat attached unavailable" id="b7-f-18" d="m 603.11133,137.66602 -95.0293,17.66601 v 64.65039 l 96.875,-10.03906 37.20899,4.72266 23.83398,3.25 v -64.6875 l -24.38281,-6.5625 z" data-id="4857" data-permalink="https://gartnerkvartalet.no/apartment/7904/"></path>
								<path class="flat attached unavailable" id="b7-f-19" d="m 603.11133,50.999997 -95.0293,25.667969 V 151.43945 L 602.88867,133.19336 666,149.04102 V 79.499997 l -24.38281,-13.5 z" data-id="4870" data-permalink="https://gartnerkvartalet.no/apartment/71004/"></path>
								<path class="flat attached unavailable" id="b7-f-20" d="m 428.18945,163.85156 -51.66601,10.16797 v 56.9375 l 51.33398,-5.40234 37.55469,5.35351 28.02344,1.38282 V 173.6875 l -27.80078,-3.00195 z" data-id="4856" data-permalink="https://gartnerkvartalet.no/apartment/7903/"></path>
								<path class="flat attached unavailable" id="b7-f-21" d="m 428.33203,229.55664 -51.66601,5.55469 V 296 L 428.83203,293.16602 465.5,294 l 28.07812,0.16602 V 236.5 l -27.80078,-1.38867 z" data-id="4850" data-permalink="https://gartnerkvartalet.no/apartment/7803/"></path>
								<path class="flat attached unavailable" id="b7-f-22" d="m 428.33203,297.22266 -51.66601,2.66601 v 58.125 l 52.16601,-0.57422 36.66797,-0.002 28.07812,0.002 V 298.22266 L 465.77734,298 Z" data-id="4842" data-permalink="https://gartnerkvartalet.no/apartment/7703/"></path>
								<path class="flat attached unavailable" id="b7-f-23" d="m 493.57812,361.55664 -27.80078,0.10938 h -37.44531 l -51.66601,0.39062 v 60.10938 l 52.16601,1 36.66797,-0.61133 28.07812,0.002 z" data-id="4835" data-permalink="https://gartnerkvartalet.no/apartment/7603/"></path>
								<path class="flat attached unavailable" id="b7-f-24" d="M 376.66602,426.22266 V 487 l 52.16601,2 36.66797,-2 28.07812,-0.86133 v -59.61133 l -27.68164,0.17383 -37.44531,0.4043 z" data-id="4828" data-permalink="https://gartnerkvartalet.no/apartment/7503/"></path>
								<path class="flat attached unavailable" id="b7-f-25" d="m 493.57812,490.08203 -27.68164,1.0293 -37.44531,1.91601 -51.78515,-2.05468 v 60.08398 L 428.83203,555.5 465.5,549.375 l 28.07812,-3.59961 z" data-id="4822" data-permalink="https://gartnerkvartalet.no/apartment/7403/"></path>
								<path class="flat attached unavailable" id="b7-f-26" d="m 493.57812,549.83203 -27.68164,3.61133 -37.44531,6.16797 -51.78515,-4.5 v 57.66601 l 52.16601,9.08985 36.66797,-9.75781 28.07812,-5.44532 z" data-id="4814" data-permalink="https://gartnerkvartalet.no/apartment/7303/"></path>
								<path class="flat attached unavailable" id="b7-f-27" d="m 493.57812,610.80859 -27.68164,5.46875 -37.44531,10 -51.78515,-9.44336 v 56.74805 l 52.16601,13.5293 36.66797,-9.81836 28.07812,-6.12891 z" data-id="4792" data-permalink="https://gartnerkvartalet.no/apartment/7203/"></path>
								<path class="flat attached unavailable" id="b7-f-28" d="m 493.57812,675.33008 -27.68164,6.08594 -37.11914,10.02734 -52.11132,-13.72656 v 5.5625 l -41.5879,9.2207 v 38.94336 L 427.33203,749.66602 465.5,739.60938 493.57812,733 Z" data-id="4766" data-permalink="https://gartnerkvartalet.no/apartment/7103/"></path>
								<path class="flat attached unavailable" id="b7-f-29" d="M 428.33203,87.749997 376.66602,103.73242 V 170 L 428,159.66602 l 37.30469,6.94531 28.27343,3.16797 v -64.44532 l -27.80078,-7.667967 z" data-id="4868" data-permalink="https://gartnerkvartalet.no/apartment/71003/"></path>
								<path class="flat attached unavailable" id="b7-f-30" d="m 293.5332,640.33398 -0.0234,54.5918 -45.5098,7.62305 82.22266,11.14258 V 689.11133 L 372,679.77734 v -27.44336 z" data-id="4757" data-permalink="https://gartnerkvartalet.no/apartment/7101/"></path>
								<path class="flat attached unavailable" id="b7-f-31" d="M 372.38672,108.5 296,121.21875 268,117.5 l -52.44531,9.16602 0.89062,27.38867 L 196,157.33203 195.55469,170 l -40.44336,5.11133 v 25.44336 l 112,-13.44336 26.66601,4.44336 78.53907,-9.77735 z" data-id="4860" data-permalink="https://gartnerkvartalet.no/apartment/71001/"></path>
								<path class="flat attached unavailable" id="b7-f-32" d="m 293.68359,584.63086 -0.15039,51.53516 78.91016,12.05664 -0.17774,-55.11328 z" data-id="4784" data-permalink="https://gartnerkvartalet.no/apartment/7201/"></path>
								<path class="flat attached unavailable" id="b7-f-33" d="M 293.5332,528.13281 V 580 l 78.91211,8.33203 V 532.5 Z" data-id="4812" data-permalink="https://gartnerkvartalet.no/apartment/7301/"></path>
								<path class="flat attached unavailable" id="b7-f-34" d="m 293.5332,470.72266 v 52.97656 l 78.91211,4.4043 v -55.38086 z" data-id="4819" data-permalink="https://gartnerkvartalet.no/apartment/7401/"></path>
								<path class="flat attached unavailable" id="b7-f-35" d="m 293.5332,415.55664 v 50.77734 l 78.91211,2.23438 v -53.01172 z" data-id="4826" data-permalink="https://gartnerkvartalet.no/apartment/7501/"></path>
								<path class="flat attached unavailable" id="b7-f-36" d="m 372.44531,359.01758 -78.91211,1.0664 V 411.5 h 78.91211 z" data-id="4833" data-permalink="https://gartnerkvartalet.no/apartment/7601/"></path>
								<path class="flat attached unavailable" id="b7-f-37" d="M 372.36133,302.50391 293.44922,305 v 51.05664 l 78.91211,-1.07031 z" data-id="4839" data-permalink="https://gartnerkvartalet.no/apartment/7701/"></path>
								<path class="flat attached unavailable" id="b7-f-38" d="m 372.36133,242.66602 -78.91211,7.72656 v 51.05273 l 78.91211,-2.91992 z" data-id="4848" data-permalink="https://gartnerkvartalet.no/apartment/7801/"></path>
								<path class="flat attached unavailable" id="b7-f-39" d="m 372.36133,185.88867 -78.91211,9.77735 v 50.66796 l 78.91211,-7.89062 z" data-id="4854" data-permalink="https://gartnerkvartalet.no/apartment/7901/"></path>
								<path class="flat attached unavailable" id="b7-f-40" d="M 665.83398,710.66602 602.88867,728.88867 537.01758,712.63672 507.5,721.38281 v 46.28321 l 35.7207,10.0039 59.44532,3.21875 63.16796,-19.11133 z" data-id="4770" data-permalink="https://gartnerkvartalet.no/apartment/7104/"></path>
							</svg>
						</div>
						<div class="back hidden" id="building-7-back">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="826.66669" width="1060" viewBox="0 0 1060 826.66669" enable-background="new 0 0 1060 826.66669" xmlSpace="preserve">
								<path class="flat attached unavailable" id="b7-b-1" d="m 54.666665,155.77802 v 54.056 L 164,207.11135 l 21.45866,-0.028 44.6,-0.416 v -59.33333 l -44.72533,2.944 -22.22267,-2.27733 z" data-id="4859" data-permalink="https://gartnerkvartalet.no/apartment/7906/"></path>
								<path class="flat attached unavailable" id="b7-b-2" d="m 54.666665,213.83335 v 55.084 l 175.167995,-2.14 0.22533,-55.85067 h -44.28266 l -22.66667,0.184 z" data-id="4853" data-permalink="https://gartnerkvartalet.no/apartment/7806/"></path>
								<path class="flat attached unavailable" id="b7-b-3" d="m 54.666665,272.83335 v 56.61067 l 175.167995,-2 0.228,-56.58134 -66.952,0.91467 z" data-id="4847" data-permalink="https://gartnerkvartalet.no/apartment/7706/"></path>
								<path class="flat attached unavailable" id="b7-b-4" d="m 54.666665,333.63868 v 56.61067 l 107.774665,0.528 H 185.556 l 44.27866,-0.66534 0.228,-58.444 -66.952,0.91467 z" data-id="4838" data-permalink="https://gartnerkvartalet.no/apartment/7606/"></path>
								<path class="flat attached unavailable" id="b7-b-5" d="m 54.666665,394.42188 v 52.68933 l 108.110665,1.44534 23.14,-0.80667 43.91733,0.028 0.228,-53.61067 -66.54933,0.58267 z" data-id="4832" data-permalink="https://gartnerkvartalet.no/apartment/7506/"></path>
								<path class="flat attached unavailable" id="b7-b-6" d="m 54.666665,450.22268 v 55.888 l 108.110665,6.056 22.77867,-2.27733 44.27733,0.22133 0.22933,-58.22 -44.396,0.0267 -22.15333,0.63866 z" data-id="4825" data-permalink="https://gartnerkvartalet.no/apartment/7406/"></path>
								<path class="flat attached unavailable" id="b7-b-7" d="m 54.666665,510.16668 v 55.056 l 108.110665,6.66667 22.556,-2.056 44.5,0.72266 0.22933,-56.16666 h -44.396 l -22.15333,1.888 z" data-id="4817" data-permalink="https://gartnerkvartalet.no/apartment/7306/"></path>
								<path class="flat attached unavailable" id="b7-b-8" d="m 54.666665,569.33334 v 52.83333 l 108.110665,9.38934 22.64,-6.056 44.61733,-0.16667 0.028,-50.668 -44.396,-0.60933 -22.15333,1.888 z" data-id="4807" data-permalink="https://gartnerkvartalet.no/apartment/7206/"></path>
								<path class="flat attached unavailable" id="b7-b-9" d="m 54.666665,626.33334 v 61.66667 l 71.999995,23.66666 38.66667,2 49.83333,-16.83333 10.67333,-48.85867 4.22267,-1.308 v -17 h -44.22933 l -23.16667,6.16667 z" data-id="4779" data-permalink="https://gartnerkvartalet.no/apartment/7106/"></path>
								<path class="flat attached unavailable" id="b7-b-10" d="m 234.05533,574.88934 19.056,0.63867 0.27467,7.612 98.77999,11.08266 38.51467,-9.01466 33.764,0.792 9.708,2.38133 0.21333,63.66 -9.94933,1.27066 -40.712,-3.36533 -31.26,9.49733 -92.36533,-16.916 -0.19067,-13.85866 -25.83333,-3.05867 z" data-id="4788" data-permalink="https://gartnerkvartalet.no/apartment/7202/"></path>
								<path class="flat attached unavailable" id="b7-b-11" d="m 245.77799,688.88934 5.376,-30.372 15.29067,-4.29467 -0.21867,-6.248 86.52,15.71467 31.75466,-9.68933 39.93734,3.37466 9.928,-1.29066 v 53.33333 l -9.92134,4.24933 -34.77733,-5.66666 -44.33333,19.33333 -111.27867,-27.66667 z" data-id="4762" data-permalink="https://gartnerkvartalet.no/apartment/7102/"></path>
								<path class="flat attached unavailable" id="b7-b-12" d="m 424.44466,125.36069 -34.61067,3.5 -37.72399,-8.51067 -91.184,14.57067 -0.31867,9.08 -26.552,2.95733 v 59.09733 l 190.65333,-16.97199 9.65733,2.63066 -0.0973,-63.104 z" data-id="4855" data-permalink="https://gartnerkvartalet.no/apartment/7902/"></path>
								<path class="flat attached unavailable" id="b7-b-13" d="m 424.44466,193.16669 -190.38933,17.22933 v 56.41067 l 118.57467,-6.47334 31.37066,0.5 40.444,-0.66666 9.892,-0.16667 0.0293,-64.052 z" data-id="4849" data-permalink="https://gartnerkvartalet.no/apartment/7802/"></path>
								<path class="flat attached unavailable" id="b7-b-14" d="m 424.44466,264.22202 -37.11067,1.11067 -34.44533,-0.33334 -118.83333,5.86267 v 56.69333 l 118.27867,-0.11066 72.11066,-0.112 9.92133,0.0467 v -63.15733 z" data-id="4841" data-permalink="https://gartnerkvartalet.no/apartment/7702/"></path>
								<path class="flat attached unavailable" id="b7-b-15" d="M 424.44466,331.52935 H 234.05533 v 58.588 h 190.38933 9.92133 v -58.588 z" data-id="4834" data-permalink="https://gartnerkvartalet.no/apartment/7602/"></path>
								<path class="flat attached unavailable" id="b7-b-16" d="m 424.44466,394.12375 -190.38933,0.0427 v 53.832 l 24.61067,0.0827 2.068,6.11867 92.6,1.46667 30.66666,-3.22267 40.444,0.44533 9.92133,0.16134 v -58.92667 z" data-id="4827" data-permalink="https://gartnerkvartalet.no/apartment/7502/"></path>
								<path class="flat attached unavailable" id="b7-b-17" d="m 424.44466,456.91668 -40.11067,-0.36133 -30.99866,3.30266 -96.02933,-2.65066 -1.52534,-4.968 -21.72533,-0.24134 v 58.28 l 22.83333,0.83334 0.0147,4.60666 96.43067,6.116 35.74933,-4.16666 35.36133,0.55466 9.92133,0.83467 v -61.996 z" data-id="4821" data-permalink="https://gartnerkvartalet.no/apartment/7402/"></path>
								<path class="flat attached unavailable" id="b7-b-18" d="m 424.44466,522.33334 -34.11067,-0.5 -38.16666,4.25067 -99.25067,-6.584 -0.33333,-4.5 -18.528,-0.58267 v 56.24933 l 23.112,0.78534 0.24933,8.21466 95.13867,10.27733 38.45733,-8.83866 33.432,0.83867 9.92133,2.376 v -61.152 z" data-id="4813" data-permalink="https://gartnerkvartalet.no/apartment/7302/"></path>
								<path class="flat attached unavailable" id="b7-b-19" d="m 438.24999,128.83335 v 63.38933 l 126.084,-3.88933 30.77733,0.33333 v -30.5 l 21.77733,-0.5 v -33.112 l -22.108,-0.10266 -0.11333,-1.78534 -16.64,1.66667 -15.36,-4.972 z" data-id="4854" data-permalink="https://gartnerkvartalet.no/apartment/7901/"></path>
								<path class="flat attached unavailable" id="b7-b-20" d="m 438.24999,196.33335 v 63.77733 l 125.084,-3.63866 15.176,0.86133 14.712,-0.36133 v -18.30534 l 23.66666,0.056 v -41.14 l -21.77733,0.084 v -4.5 z" data-id="4848" data-permalink="https://gartnerkvartalet.no/apartment/7801/"></path>
								<path class="flat attached unavailable" id="b7-b-21" d="m 438.24999,264.22202 v 63.77733 l 125.41733,-1.77733 15.22133,-0.22267 14.33334,-0.22266 v -16.73467 l 23.66666,0.0413 v -32.64 l -23.444,-0.11066 -0.22133,-15.27734 z" data-id="4839" data-permalink="https://gartnerkvartalet.no/apartment/7701/"></path>
								<path class="flat attached unavailable" id="b7-b-22" d="m 438.24999,331.83335 v 58.284 l 125.30533,1.66 h 16.88933 12.77734 v -11.944 l 23.66666,0.16667 v -33.00134 l -23.444,0.003 -0.22133,-16.89067 z" data-id="4833" data-permalink="https://gartnerkvartalet.no/apartment/7601/"></path>
								<path class="flat attached unavailable" id="b7-b-23" d="m 438.24999,394.12375 v 58.76533 l 125.30533,2.84 15.44533,-1.06266 14.22134,-0.001 v -5.444 l 23.66666,0.056 v -31.72266 l -22.22133,-0.444 -0.112,-20.94534 -31.884,0.33467 z" data-id="4826" data-permalink="https://gartnerkvartalet.no/apartment/7501/"></path>
								<path class="flat attached unavailable" id="b7-b-24" d="m 438.24999,457.25001 v 61.91733 l 123.48933,4.63867 17.26133,-2.51467 14.22134,0.152 23.66666,0.36134 v -32.472 l -22.16533,0.16666 -0.112,-30.66666 -15.19467,-0.0827 -16.42,1.08 z" data-id="4819" data-permalink="https://gartnerkvartalet.no/apartment/7401/"></path>
								<path class="flat attached unavailable" id="b7-b-25" d="m 438.24999,523.33334 v 61.66667 l 123.48933,6.66666 17.26133,-1.11067 37.888,0.332 v -30.38799 l -22.16533,-0.27734 -0.112,-34.83333 -15.19467,-0.084 -16.972,2.69467 z" data-id="4812" data-permalink="https://gartnerkvartalet.no/apartment/7301/"></path>
								<path class="flat attached unavailable" id="b7-b-26" d="m 438.24999,589.00001 v 63 l 125.66666,9.16666 15.084,-3.83333 37.888,1.584 v -28.41733 l -22.16533,0.003 -0.112,-35.50267 -15.19467,-0.16667 -16.972,1 z" data-id="4784" data-permalink="https://gartnerkvartalet.no/apartment/7201/"></path>
								<path class="flat attached unavailable" id="b7-b-27" d="m 438.24999,656.11067 v 63 l 121.032,18.06533 36.38533,-18.84133 -0.12267,-50.89067 11.56667,-4.888 -12.5,-0.44533 -15.19467,-0.16667 -16.972,3.38934 z" data-id="4757" data-permalink="https://gartnerkvartalet.no/apartment/7101/"></path>
								<path class="flat attached unavailable" id="b7-b-28" d="m 995.58064,76.110022 v -16.5 L 966.91397,35.77669 792.91398,55.110023 v -6.333334 l -12.66667,1.333334 -0.33333,-2 -28.66667,-15 -26.66666,2.666667 -6.66667,-4.333334 h -8.33333 l -33,-15.999999 -213,31.333332 -36.74533,-13.474666 -192.77999,29.253333 v 80.277334 l 22.48666,-2.54134 0.208,-8.87066 95.64533,-14.796 37.54934,8.208 34.94399,-3.556 11.69467,3.80666 126.416,-9.86133 15.45866,5.028 198.37467,-17.91733 205.83332,-20.000001 15.58134,9.943999 V 78.776689 Z" data-id="4864" data-permalink="https://gartnerkvartalet.no/apartment/71002/"></path>
								<path class="flat attached unavailable" id="b7-b-29" d="M 187.55533,70.222689 54.666,89.777355 v 61.889335 l 108,-7.66667 22.584,2.584 44.72533,-3.33333 0.084,-80.088001 -42.504,6.449333 z" data-id="4883" data-permalink="https://gartnerkvartalet.no/apartment/71006/"></path>
							</svg>
						</div>

					</div>
					<div class="aside">
						<div class="inner">
							<h2>Gjennomgående 94,5 m² leilighet med to balkonger og mulighet for hybel</h2>
							<h3>Leilighet <strong>8404</strong></h3>
							<h3>Leilighetstype <strong>Q</strong></h3>
							<p><strong>Totalpris:</strong> 8.150.000,-</p>
							<p><strong>Antall rom:</strong> 4</p>
							<p><strong>Etasje:</strong> 4</p><p><strong>BRA:</strong> 94 m²</p>
							<p><strong>P-rom:</strong> 91 m²</p><p><strong>Balkong:</strong> 8,5  m² +  4,2 m²</p>
							<p><strong>Parkeringsplass:</strong> Ja</p><p><strong>Innflytting</strong> 25.05.21 - 25.08.21</p><div class="floor-plan">
								<img src="https://gartnerkvartalet.no/wp-content/uploads/2019/02/8204-8604-1-320x260.jpg" alt="Floor plan" />
							</div>
						</div>
					</div>
				</div>


			</div>
		</div >
	);
}

// import React, { Component } from "react";
// import ImageMap from "image-map";

// ImageMap('img[usemap]');

// const URL = "https://c1.staticflickr.com/5/4052/4503898393_303cfbc9fd_b.jpg"
// const MAP = {
// 	name: "my-map",
// 	areas: [
// 		{ name: "1", shape: "poly", coords: [25, 33, 27, 300, 128, 240, 128, 94], preFillColor: "green", fillColor: "blue" },
// 		{ name: "2", shape: "poly", coords: [219, 118, 220, 210, 283, 210, 284, 119], preFillColor: "pink" },
// 		{ name: "3", shape: "poly", coords: [381, 241, 383, 94, 462, 53, 457, 282], fillColor: "yellow" },
// 		{ name: "4", shape: "poly", coords: [245, 285, 290, 285, 274, 239, 249, 238], preFillColor: "red" },
// 		{ name: "5", shape: "circle", coords: [170, 100, 25] },
// 	]
// }
// export default class ImageMapper extends Component {

// 	constructor(props) {

// 		super(props);
// 		this.state = {
// 			hoveredArea: {},
// 		};
// 	}

// 	selectRegion(ctx, region) {
// 		var selector = 'img[usemap=\'#' + ctx.parentElement.name + '\']';
// 		document.querySelector(selector).setAttribute('src', 'image-map-' + region + '.png');
// 	}

// 	enterArea(area) {
// 		this.setState({ hoveredArea: area });
// 	}

// 	leaveArea(area) {
// 		this.setState({ hoveredArea: null });
// 	}

// 	getTipPosition(area) {
// 		return { top: `${area.center[1]}px`, left: `${area.center[0]}px` };
// 	}


// 	render() {
// 		return (
// 			<>
// <div id="greenhouse">
//     <svg id="lines" viewBox="0 0 1280 1280" 
// preserveAspectRatio="xMinYMin meet">
//         <path stroke="#d4ffde" stroke-width="3" fill="none"  d="
//             M 130,820
//             L 130,320
//             l 50,0
//         "/>
//         <path stroke="#d4ffde" stroke-width="3" fill="none" d="
//             M 620,820
//             L 620,520
//             l -230,0
//         "/>
//         <path stroke="#d4ffde" stroke-width="3" fill="none" d="
//             M 1100,820
//             L 1100,380                         
//             l -290,0
//         "/>
//     </svg>
//     <svg viewBox="0 0 1280 720" 
//     preserveAspectRatio="xMinYMin meet">
//         <image width="1280" 
//         height="720" xlink="https://dkli3tbfz4zj3.cloudfront.net/all/202006_SaladDays/images/greenhouse.jpg">
//         </image>
//         <circle cx="220" cy="320" r="40" stroke="#d4ffde" 
//         stroke-width="3" fill="none" />
//         <circle cx="320" cy="520" r="70" stroke="#d4ffde" 
//         stroke-width="3" fill="none" />
//         <circle cx="750" cy="380" r="60" stroke="#d4ffde" 
//         stroke-width="3" fill="none" />
//     </svg>
//     <div id="greenhouse-details">
//         <div class="narrow-text">
//             <p>Whip up a delicious salad</p>
//         </div>
//         <div class="narrow-text">
//             <p>Cuddle up to Marguerit’s Snow Stalker companion</p>
//         </div>
//         <div class="narrow-text">
//             <p>Learn how to farm new, unusual plants</p>
//         </div>
//     </div>
// </div>
// 				{/* <img usemap="#image-map" src="https://www.travismclarke.com/imagemap/image-map-all.png" onClick={()=>this.selectRegion(this, 'yellow')} />

// 				<map name="image-map">
// 					<area shape="poly" coords="22,22,231,22,264,82,232,143,22,143" />
// 					<area shape="poly" coords="233,22,443,22,476,82,442,144,233,143,264,82" />
// 					<area shape="poly" coords="445,22,654,22,686,81,654,143,444,143,475,82" />
// 					<area shape="poly" coords="655,22,895,22,895,142,655,142,684,82" />
// 				</map> */}

// 			</>);
// 	}


// };*/